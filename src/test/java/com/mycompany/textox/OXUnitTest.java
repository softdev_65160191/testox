/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.textox;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Chantima
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckRow2_O_output_true() {
        String [][] table = {{"1","2","3"},{"O","O","O"},{"7","8","9"}};
        String currentPlayer ="O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckRow1_O_output_true() {
        String [][] table = {{"O","O","O"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer ="O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckRow3_O_output_true() {
        String [][] table = {{"1","2","3"},{"4","5","6"},{"O","O","O"}};
        String currentPlayer ="O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckRow1_X_output_true() {
        String [][] table = {{"X","X","X"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer ="X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckRow2_X_output_true() {
        String [][] table = {{"1","2","3"},{"X","X","X"},{"7","8","9"}};
        String currentPlayer ="X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckRow3_X_output_true() {
        String [][] table = {{"1","2","3"},{"4","5","6"},{"X","X","X"}};
        String currentPlayer ="X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckRow3_X_output_false() {
        String [][] table = {{"1","2","3"},{"4","5","6"},{"X","X","9"}};
        String currentPlayer ="X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(false,result);
    }
     @Test
    public void testCheckRow2_X_output_false() {
        String [][] table = {{"1","2","3"},{"X","X","6"},{"7","8","9"}};
        String currentPlayer ="X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckRow1_X_output_false() {
        String [][] table = {{"X","2","X"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer ="X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(false,result);
    }
   
    
}
